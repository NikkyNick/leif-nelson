$(document).ready(function(){
	//	custom selecmenu
	$('select').selectmenu();
	//		show vacancy information
	$('#descr_list').on('click', '.header', function(){
		$(this).toggleClass('open');
		$(this).parent().children('.content').stop().slideToggle(300);
	});
});
$(window).load(function() {
	//		init sliders
	$('.small_slider').flexslider({
		animation: "slide",
		animationLoop: true,
		itemWidth: 153,
		minItems: 3,
		maxItems: 3,
		controlNav: false,
		directionNav: true
	});
});